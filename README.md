[![header][header-url]][header-link]

# Nova Space Program

[![Project Version][version-image]][version-url]
[![Frontend][Frontend-image]][Frontend-url]
[![Backend][Backend-image]][Backend-url]

---

## Author

**GoFlood community** 

* *Initial work* - [repository-name][repository-url] (Repository space)
* *Released on* [cloud-provider][cloud-provider-url] (Cloud provider)

## Showcase

This project was designed to demonstrate:

* Blueprint comunication
* Modeling skills
* Team work
* Why UE is better than Unity 😉

---

## Development setup

verify you have Git installed on your computer by typing `git -v` in your favorite terminal (CMD / Powershell / Hyper / Terminator)

If an error is prompt, you have to install [Git](https://git-scm.com/downloads) before continue

```bash
# Go to your projects main directory
cd C:\Users\<YOUR_USERNAME>\Documents

# Clone the prject
git clone https://gitlab.com/hizenjs/unreal-community-project.git defi-frontiere

# Init your git profile
git config --global user.email "any@email.adress"
git config --global user.name "Your_Username"

# Make sur to have the latest changes by pulling the repository
git pull origin main

# Create a new branch for your work
git checkout -b your-branch-name

# WORK !!!

# Once you have finish a task, push your work using:
# Add everything
git add .
# Add a message for your commit
git commit -m "feat: your feature commit message"
git push origin your-branch-name

# Then you can merge your task in the main project
git checkout main
git pull origin main
git merge your-branch-name

# Then everything is done. You can recreate a new branch again and start working
```

<!-- Markdown link & img dfn's -->

[header-url]: https://i.ibb.co/nc9rFWD/nsp.png
[header-link]: https://app.mural.co/t/nexrproject2798/m/nexrproject2798/1679315496691/e76164ef1ab47f16c8e885223752966d94313a53?invited=true&sender=u2ad7fd06c8d3ad2624f14888

[repository-url]: https://gitlab.com/hizenjs/unreal-community-project

[cloud-provider-url]: https://store.epicgames.com/fr/

[ue-profile]: https://dev.epicgames.com/community/profile/7eD6/Hizen

[wiki]: https://docs.unrealengine.com/5.1/en-US/

[version-image]: https://img.shields.io/badge/Version-0.1.4-purple?style=for-the-badge&logo=unreal-engine
[version-url]: https://img.shields.io/badge/version-0.1.4-green
[Frontend-image]: https://img.shields.io/badge/Frontend-Blueprint-blue?style=for-the-badge
[Frontend-url]: https://img.shields.io/badge/Frontend-Blueprint-blue?style=for-the-badge
[Backend-image]: https://img.shields.io/badge/Backend-UE5%20C++-important?style=for-the-badge
[Backend-url]: https://img.shields.io/badge/Backend-UE5%20C++-important?style=for-the-badge
